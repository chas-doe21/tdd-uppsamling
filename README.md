# Test Driven Development

Project: a cli-based task manager (todo list).

## Requirements

- as self-contained as possible
- developed using TDD
- pytest + pytest-cov

