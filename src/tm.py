#!/usr/bin/env python3

'''tm: a cli task manager (todo list) developed using TDD.
'''

import json
import os
import sys

USAGE = '''tm.py - a python cli based task manager.
USAGE:    tm.py [command] [args]
'''

TASKLIST = "tasks.json"


def load_tasklist():
    '''load tasks from TASKLIST
    '''
    if os.path.isfile(TASKLIST):
        with open(TASKLIST) as file:
            return json.load(file)
    else:
        return []


def save_tasklist(tasklist):
    '''Dump tasklist as json to a file (TASKLIST)
    '''
    with open(TASKLIST, "w") as file:
        json.dump(tasklist, file, indent=4)


def cmd_add(argv):
    '''Add a new task to the tasklist
    '''
    if len(argv) <= 2:
        print("Refusing to create an empty task", file=sys.stderr)
        return
    tasks = load_tasklist()
    tasks.append(" ".join(argv[2:]))
    save_tasklist(tasks)


def cmd_list(argv):
    '''List all the tasks
    '''
    tasks = load_tasklist()
    if not tasks:
        print("You have no tasks to do.", file=sys.stderr)
        return
    print("TO DO:")
    for idx, task in enumerate(tasks, start=1):
        print(f"    {idx}. {task}")


def cmd_done(argv):
    if len(argv) <= 2:
        print("Missing index to mark as done.", file=sys.stderr)
        return
    idx = int(argv[2])
    tasks = load_tasklist()
    # Check if index is in the right range
    if idx < 0:
        print("Can't handle negative indexes.", file=sys.stderr)
        return
    if idx > len(tasks):
        print("Task id not found.", file=sys.stderr)
        return
    del tasks[idx-1]
    save_tasklist(tasks)


def main(argv):
    '''The script's entrypoint. Parse a command and trigger it, if needed show
    the usage message.
    '''
    if len(argv) <= 1:
        print(USAGE, file=sys.stderr)
        return
    if argv[1] == "add":
        cmd_add(argv)
        return
    elif argv[1] == "list":
        cmd_list(argv)
    elif argv[1] == "done":
        cmd_done(argv)
    else:
        print(USAGE, file=sys.stderr)
        return


if __name__ == '__main__':
    main(sys.argv)
