'''Unit tests for tm.py
'''

import json
import os

from .tm import TASKLIST, USAGE, load_tasklist, main, save_tasklist


def test_cmd_list_incomplete(tmp_path, capsys):
    os.chdir(tmp_path)
    main(["", "done"])
    captured = capsys.readouterr()
    assert captured.err == "Missing index to mark as done.\n"


def test_cmd_done(tmp_path):
    os.chdir(tmp_path)
    save_tasklist(["task one", "task two"])
    main(["", "done", "1"])
    assert load_tasklist() == ["task two"]


def test_cmd_done_wrong_idx(tmp_path, capsys):
    os.chdir(tmp_path)
    save_tasklist(["task one", "task two"])
    main(["", "done", "10"])
    captured = capsys.readouterr()
    assert captured.err == "Task id not found.\n"


def test_cmd_done_negative_idx(tmp_path, capsys):
    os.chdir(tmp_path)
    save_tasklist(["task one", "task two"])
    main(["", "done", "-1"])
    captured = capsys.readouterr()
    assert captured.err == "Can't handle negative indexes.\n"


def test_cmd_list_empty(capsys, tmp_path):
    os.chdir(tmp_path)
    main(["", "list"])
    captured = capsys.readouterr()
    assert captured.err == "You have no tasks to do.\n"


def test_cmd_list(capsys, tmp_path):
    os.chdir(tmp_path)
    save_tasklist(["task one"])
    main(["", "list"])
    captured = capsys.readouterr()
    assert captured.out == """TO DO:
    1. task one
"""


def test_save_tasklist_empty(tmp_path):
    os.chdir(tmp_path)
    save_tasklist([])
    assert os.path.isfile(TASKLIST)
    assert load_tasklist() == []


def test_save_tasklist(tmp_path):
    os.chdir(tmp_path)
    tasks = ["one", "two"]
    save_tasklist(tasks)
    assert load_tasklist() == tasks


def test_load_tasklist_empty(tmp_path):
    os.chdir(tmp_path)
    assert load_tasklist() == []


def test_load_taskslist(tmp_path):
    tasks = ["task one", "task two"]
    os.chdir(tmp_path)
    # Create a tasks.json with content
    with open(TASKLIST, "w") as file:
        json.dump(tasks, file)
    # assert the tasks have been loaded correctly
    assert load_tasklist() == tasks


def test_no_command(capsys):
    '''Test main when no command is given.
    '''
    main([""])
    captured = capsys.readouterr()
    assert captured.err == USAGE + "\n"


def test_empty_argv(capsys):
    '''Test main when an empty argv is given.
    '''
    main([])
    captured = capsys.readouterr()
    assert captured.err == USAGE + "\n"


def test_cmd_add(tmp_path):
    '''Test main when giving "add" as a command
    '''
    os.chdir(tmp_path)
    main(["", "add", "new", "task", "here"])
    assert load_tasklist() == ["new task here"]


def test_cmd_add_empty(capsys, tmp_path):
    os.chdir(tmp_path)
    main(["", "add"])
    captured = capsys.readouterr()
    assert captured.err == "Refusing to create an empty task\n"
    assert load_tasklist() == []


def test_cmd_not_found(capsys):
    '''Test main when giving an unknown command
    '''
    main(["", "qwerty"])
    captured = capsys.readouterr()
    assert captured.err == USAGE + "\n"
